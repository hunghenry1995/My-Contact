package com.contact.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.contact.entity.MyContact;

@Service
public interface ContactService {
	Optional<MyContact> findByid(int id);

	List<MyContact> findAll();

	void save(MyContact contact);

//parameter of delete is Integer because default of method delete in crud repository is number 0 
//	so we dont know object is null or not (Wrapper class)
	void delete(Integer id);

	void update(MyContact contact);
}
