package com.contact.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "MyContact")
@Table(name = "MyContact")
public class MyContact {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer contactId;
	@Column(name = "ContactName")
	private String contactName;

	public MyContact() {

	}

	
	public MyContact(Integer contactId, String contactName) {
		super();
		this.contactId = contactId;
		this.contactName = contactName;
	}


	public Integer getContactId() {
		return contactId;
	}


	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}


	public String getContactName() {
		return contactName;
	}


	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	@Override
	public String toString() {
		return "ContactName:" + "(" + this.contactName + ")";

	}
}
