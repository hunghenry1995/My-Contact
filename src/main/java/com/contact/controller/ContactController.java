package com.contact.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.contact.entity.MyContact;
import com.contact.serviceImp.ContactServiceImp;

@Controller
public class ContactController {

	@Autowired
	ContactServiceImp contactService;

	@RequestMapping("/contact")
	public String showContact(Model model) {

		model.addAttribute("contacts", contactService.findAll());
		List<MyContact> length = contactService.findAll();

		System.out.println(length.size());

		return "contact";
	}

	@RequestMapping(value = "/addContact", method = RequestMethod.GET)
	public String showAddForm(Model model) {
		model.addAttribute("Contact", new MyContact());
		return "addContact";
	}

	@RequestMapping(value = "/addContact", method = RequestMethod.POST)
	public String addContact(Model model, @ModelAttribute("Contact") MyContact contact) {
		contactService.save(contact);
		return "redirect:contact";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteContact(@PathVariable("id") Integer id, Model model) {
		System.out.println("" + id);
		contactService.delete(id);
		model.addAttribute("contacts", contactService.findAll());
		return showContact(model);
	}

//	@RequestMapping(value = "/aa", method = RequestMethod.GET)
//	public String ai() {
//		Integer id = 3;
//		contactService.delete(id);
//		return "redirect:contact";
//	}
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public String updateContact(@PathVariable("id") Integer id, Model model) {
		MyContact contact = contactService.findByid(id).orElse(null);
		contactService.save(contact);
		model.addAttribute("contacts", contact);
		return "update";
	}
}
