package com.contact.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.contact.entity.MyContact;

@Repository
public interface ContactRepository extends JpaRepository<MyContact, Integer> {
//	findbynamewithlike is select * from Contact where name like %name%
//	but dont know why we dont write in Service like any method else?
//	List<MyContact> findByNameWithLike(String name);

}
