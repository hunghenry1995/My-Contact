package com.contact.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.contact.entity.MyContact;
import com.contact.repository.ContactRepository;
import com.contact.service.ContactService;

@Service
public class ContactServiceImp implements ContactService {
	@Autowired
	ContactRepository repo;

	@Override
	public Optional<MyContact> findByid(int id) {
		// TODO Auto-generated method stub
		return repo.findById(id);

	}

	@Override
	public List<MyContact> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public void save(MyContact contact) {
		// TODO Auto-generated method stub
		repo.save(contact);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		MyContact contact = findByid(id).orElse(null);
		if (contact == null) {
			throw new IllegalArgumentException("contact object is not exists");
		}
		repo.delete(contact);
	}

	@Override
	public void update(MyContact contact) {
		// TODO Auto-generated method stub
		if (contact.getContactId() == 0) {
			throw new IllegalArgumentException("could not identified this Id to update");
		}
		repo.save(contact);
	}
}