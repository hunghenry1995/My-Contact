package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.contact.controller","com.contact.service","com.contact.serviceImp" })
@EntityScan("com.contact.entity")
@EnableJpaRepositories({"com.contact.repository"})

//@ComponentScan({"com.contact.controller","com.contact.entity","com.contact.serviceImp","com.contact.repository"})
public class MyContactApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyContactApplication.class, args);
	}

}
