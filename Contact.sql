use master
go
if exists (select * from sys.databases where name = 'Contact')
drop database Contact
go
create database Contact
go
use Contact
go
create table MyContact
(
ContactId int identity ,
ContactName nvarchar(50),
)
go
create table ContactDetail
(
DetailId int primary key,
FirstName nvarchar(50),
LastName nvarchar(50),
PhoneNumber nvarchar(50),
ContactId int
)
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into MyContact values(N'A.Hung')
insert into ContactDetail values(1,N'Nguyen Dang',N'Hung','0939283123',1)
insert into ContactDetail values(2,N'Nguyen Dang',N'Hung','0939283123',2)

select * from MyContact
select * from ContactDetail
delete MyContact where ContactId = 1
